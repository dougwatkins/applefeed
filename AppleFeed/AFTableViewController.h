//
//  AFTableViewController.h
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSItem.h"

@interface AFTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) BOOL isApps;
@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

-(void) typeSwitched;
-(void) loadNewData;

@end
