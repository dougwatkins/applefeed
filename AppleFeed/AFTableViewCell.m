//
//  AFTableViewCell.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "AFTableViewCell.h"

@implementation AFTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
