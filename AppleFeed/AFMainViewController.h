//
//  AFMainViewController.h
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFTableViewController.h"

@interface AFMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *feedTypeSegment;
@property (nonatomic, strong) AFTableViewController *feedView;

- (IBAction)feedTypeChanged;

@end
