//
//  AFEntry.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/20/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "AFFeed.h"
#import "RSSItem.h"

@implementation AFFeed

-(id) init {
	self = [super init];
	if (self) {
		self.items = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
//	NSLog(@"Element - %@", elementName);
	if ([elementName isEqualToString:@"entry"]) {
		RSSItem *item = [[RSSItem alloc] init];
		[parser setDelegate:item];
		[item setParent:self];
		[self.items addObject:item];
	}
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	if ([elementName isEqualToString:@"entry"]) {
		[parser setDelegate:self.parent];
	}
}

@end
