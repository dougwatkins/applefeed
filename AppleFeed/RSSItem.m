//
//  AFItem.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "RSSItem.h"

@implementation RSSItem

- (void) encodeWithCoder:( NSCoder *) aCoder {
	
	[aCoder encodeObject:_title forKey:@"title"];
	[aCoder encodeObject:_summary forKey:@"summary"];
	[aCoder encodeObject:_linkURL forKey:@"linkURL"];
	[aCoder encodeObject:_imageURL forKey:@"imageURL"];
}

- (id) initWithCoder:( NSCoder *) aDecoder {
	self = [super init];
	if (self) {
		self.title = [aDecoder decodeObjectForKey:@"title"];
		self.summary = [aDecoder decodeObjectForKey:@"summary"];
		self.linkURL = [aDecoder decodeObjectForKey:@"linkURL" ];
		self.imageURL = [aDecoder decodeObjectForKey:@"imageURL"];
		
	}
	return self;
}

-(UIImage *) getImageThumb {
	if (!self.imageThumb) {
		self.imageThumb = [[UIImage alloc] init];
		NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
		self.imageThumb = [self loadImage:self.title inDirectory:documentDirectory];
	}

	return self.imageThumb;
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
//	NSLog(@"element = %@", elementName);
	if ([elementName isEqualToString:@"title"]) {
		currentString = [[NSMutableString alloc] init];
		self.title = currentString;
	} else if ([elementName isEqualToString:@"summary"]) {
		currentString = [[NSMutableString alloc] init];
		self.summary = currentString;
	} else if ([elementName isEqualToString:@"im:image"]) {
		if ([[attributeDict objectForKey:@"height"] isEqualToString:@"100"] || [[attributeDict objectForKey:@"height"] isEqualToString:@"170"]) {
			currentString = [[NSMutableString alloc] init];
			self.imageURL = currentString;
		}
	} else if ([elementName isEqualToString:@"link"]) {
		self.linkURL = [attributeDict objectForKey:@"href"];
	}
}

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
	currentString = nil;
	if ([elementName isEqualToString:@"entry"]) {
		[parser setDelegate:self.parent];
		NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
		[self saveImage:[self getImageFromURL:self.imageURL] withFileName:self.title inDirectory:documentDirectory];
	}
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
	if (currentString) {
		[currentString appendString:string];
	}
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
	UIImage * result;
	
	NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
	result = [UIImage imageWithData:data];
	
	return result;
}

-(UIImage *) loadImage:(NSString *)fileName inDirectory:(NSString *)directoryPath {
	UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", directoryPath, fileName]];
	
	return result;
}

-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName inDirectory:(NSString *)directoryPath {
	NSError *err;
	[UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]] options:NSAtomicWrite error:&err];
	if (err) {
		NSString *errorString = [NSString stringWithFormat:@" Image save failed: %@", [err localizedDescription]];
		
		UIAlertView *alert = [[ UIAlertView alloc] initWithTitle:@" Couldnt save image"
														 message:errorString
														delegate:nil
											   cancelButtonTitle:@" OK"
											   otherButtonTitles:nil];
		[alert show];

	}
}

@end
