//
//  AFItem.h
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RSSItem : NSObject <NSCoding, NSXMLParserDelegate>
{
	NSMutableString *currentString;
}

@property (nonatomic, strong) UIImage *imageThumb;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *linkURL;
@property (nonatomic, strong) id parent;


-(UIImage *) getImageFromURL:(NSString *)fileURL;
-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName inDirectory:(NSString *)directoryPath;
-(UIImage *) getImageThumb;
-(UIImage *) loadImage:(NSString *)fileName inDirectory:(NSString *)directoryPath;

@end
