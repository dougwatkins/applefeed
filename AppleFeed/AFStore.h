//
//  AFStore.h
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFFeed.h"

@interface AFStore : NSObject <NSXMLParserDelegate>
{
	NSURLConnection *connection;
	NSMutableData *xmlData;
	BOOL isApps;
}

@property (nonatomic, strong) NSArray *apps;
@property (nonatomic, strong) NSArray *podcasts;
@property (nonatomic, strong) AFFeed *feed;

+(AFStore *) sharedStore;
-(NSArray *) getApps;
-(NSArray *) getPodcasts;
-(void) downloadApps;
-(void) downloadPodcasts;
-(BOOL) readAppsFromDisk;
-(BOOL) readPodcastsFromDisk;
-(void) saveData;
- (NSString *) itemArchivePathPods;
- (NSString *) itemArchivePathApps;

@end
