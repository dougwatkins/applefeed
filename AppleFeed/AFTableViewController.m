//
//  AFTableViewController.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "AFTableViewController.h"
#import "AFTableViewCell.h"
#import "RSSItem.h"
#import "AFStore.h"

@interface AFTableViewController ()

@end

@implementation AFTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.isApps = YES;
	UINib *nib = [UINib nibWithNibName:@"AFTableViewCell" bundle:nil];
	[[self tableView] registerNib:nib forCellReuseIdentifier:@"AppleFeedCell"];
	self.refreshControl = [[UIRefreshControl alloc] init];
	self.refreshControl.backgroundColor = [UIColor clearColor];
	self.refreshControl.tintColor = [UIColor lightGrayColor];
	[self.refreshControl addTarget:self
							action:@selector(loadNewData)
				  forControlEvents:UIControlEventValueChanged];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typeSwitched) name:@"finishedLoading" object:nil];
	[self typeSwitched];

}

-(void) dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) loadNewData {
	if (self.isApps) {
		[[AFStore sharedStore] downloadApps];
	} else {
		[[AFStore sharedStore] downloadPodcasts];
	}
}

-(void) typeSwitched {
	if (self.isApps) {
		self.tableData = [[AFStore sharedStore] getApps];
	} else {
		self.tableData = [[AFStore sharedStore] getPodcasts];
	}
	[self reloadData];
}

- (void)reloadData
{
		// Reload table data
	[self.tableView reloadData];
	
		// End the refreshing
	if (self.refreshControl) {
		
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"MMM d, h:mm a"];
		NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
		NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]
																	forKey:NSForegroundColorAttributeName];
		NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
		self.refreshControl.attributedTitle = attributedTitle;
		[self.refreshControl endRefreshing];

	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.tableData.count;
//	return 25;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 116;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppleFeedCell" forIndexPath:indexPath];
    
    // Configure the cell...
	RSSItem *item = [self.tableData objectAtIndex:[indexPath row]];
	cell.appIconImageView.image = [item getImageThumb];
	cell.titleLabel.text = item.title;
	cell.summaryLabel.text = item.summary;
	
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	RSSItem *item = [self.tableData objectAtIndex:[indexPath row]];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.linkURL]];
}

@end
