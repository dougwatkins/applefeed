//
//  AFStore.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "AFStore.h"
#import <UIKit/UIKit.h>

@implementation AFStore

-(id) init {
	self = [super init];
	if (self) {
		if (![self readAppsFromDisk]) {
			[self downloadApps];
		}
	}
	return self;
}

-(void) saveData {
	NSString *pathPods = [self itemArchivePathPods];
	
	if (![NSKeyedArchiver archiveRootObject:self.podcasts toFile:pathPods]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to save podcasts."
														message:@"Archiving failed"
													   delegate:self
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}
	NSString *pathApps = [self itemArchivePathApps];
	
	if (![NSKeyedArchiver archiveRootObject:self.apps toFile:pathApps]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to save apps."
														message:@"Archiving failed"
													   delegate:self
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}

}

+(AFStore *) sharedStore {
	static AFStore *sharedStore = nil;
	
	if (!sharedStore) {
		sharedStore = [[AFStore alloc] init];
	}
	return sharedStore;
}

-(NSArray *) getApps {
	if (!self.apps) {
		isApps = YES;
		[self downloadApps];
	}
	return self.apps;
}

-(NSArray *) getPodcasts {
	if (!self.podcasts) {
		isApps = NO;
		if (![self readPodcastsFromDisk]) {
			[self downloadPodcasts];
		}
	}
	return self.podcasts;
}

- (void) connection:( NSURLConnection *) conn didReceiveData:( NSData *) data {
	[xmlData appendData:data];
}

- (void) connectionDidFinishLoading:( NSURLConnection *) conn {
//	NSString *xmlCheck = [[ NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
//	NSLog(@" xmlCheck = %@", xmlCheck);
	
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:xmlData];
	[parser setDelegate:self];
	
	[parser parse];
	xmlData = nil;
	connection = nil;
	if (isApps) {
		self.apps = self.feed.items;
	} else {
		self.podcasts = self.feed.items;
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"finishedLoading" object:nil];
}
- (void) connection:( NSURLConnection *) conn didFailWithError:( NSError *) error {
	connection = nil;
	xmlData = nil;
	
	NSString *errorString = [NSString stringWithFormat:@" Fetch failed: %@", [error localizedDescription]];
	
	UIAlertView *alert = [[ UIAlertView alloc] initWithTitle:@" Error"
												  message:errorString
												 delegate:nil
										   cancelButtonTitle:@" OK"
										   otherButtonTitles:nil];
	[alert show];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"finishedLoading" object:nil];
}


-(void) parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	NSString *errorString = [NSString stringWithFormat:@" Parse failed: %@", [parseError localizedDescription]];
	
	UIAlertView *alert = [[ UIAlertView alloc] initWithTitle:@" Parsing Failed."
												  message:errorString
												 delegate:nil
										   cancelButtonTitle:@" OK"
										   otherButtonTitles:nil];
	[alert show];

}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
		//	NSLog(@"Element - %@", elementName);
	if ([elementName isEqualToString:@"feed"]) {
		self.feed = [[AFFeed alloc] init];
		[parser setDelegate:self.feed];
		[self.feed setParent:self];
	}
}

-(void) downloadPodcasts {
	isApps = NO;
	xmlData = [[NSMutableData alloc] init];
	NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/rss/toppodcasts/limit=25/xml"];
	NSURLRequest *req = [NSURLRequest requestWithURL:url];
 	connection = [[ NSURLConnection alloc] initWithRequest:req delegate:self startImmediately:YES];
	
}

-(void) downloadApps {
	isApps = YES;
	xmlData = [[NSMutableData alloc] init];
	NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/rss/topfreeapplications/limit=25/xml"];
	NSURLRequest *req = [NSURLRequest requestWithURL:url];
	connection = [[ NSURLConnection alloc] initWithRequest:req delegate:self startImmediately:YES];
	
}

-(BOOL) readAppsFromDisk {
	NSString *pathApps = [self itemArchivePathApps];
	self.apps = [NSKeyedUnarchiver unarchiveObjectWithFile:pathApps];
	if (!self.apps) {
		self.apps = [[NSArray alloc] init];
		return NO;
	}
	return YES;
}

-(BOOL) readPodcastsFromDisk {
	NSString *pathPods = [self itemArchivePathPods];
	self.podcasts = [NSKeyedUnarchiver unarchiveObjectWithFile:pathPods];
	if (!self.podcasts) {
		self.podcasts = [[ NSArray alloc] init];
		return NO;
	}
	
	return YES;
}

- (NSString *) itemArchivePathPods {
	NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
	return [documentDirectory stringByAppendingPathComponent:@"podcasts.archive"];
}

- (NSString *) itemArchivePathApps {
	NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDirectory = [documentDirectories objectAtIndex: 0];
	return [documentDirectory stringByAppendingPathComponent:@"apps.archive"];
}

@end
