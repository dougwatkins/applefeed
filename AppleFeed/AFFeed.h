//
//  AFEntry.h
//  AppleFeed
//
//  Created by Doug Watkins on 3/20/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSItem.h"

@interface AFFeed : NSObject <NSXMLParserDelegate>


@property (nonatomic, strong) NSMutableArray *items;
//@property (nonatomic, strong) AFItem *item;
@property (nonatomic, strong) id parent;

@end
