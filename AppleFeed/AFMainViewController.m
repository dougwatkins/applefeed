//
//  AFMainViewController.m
//  AppleFeed
//
//  Created by Doug Watkins on 3/19/15.
//  Copyright (c) 2015 Doug Watkins. All rights reserved.
//

#import "AFMainViewController.h"

@interface AFMainViewController ()

@end

//const int SegmentSize = 29;
const int Padding = 8;

@implementation AFMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	if (!self.feedView) {
		self.feedView = [[AFTableViewController alloc] initWithStyle:UITableViewStylePlain];
	}
	self.feedView.view.frame = CGRectMake(0, self.feedTypeSegment.frame.size.height + 3*Padding, self.view.frame.size.width, self.view.frame.size.height- self.feedTypeSegment.frame.size.height - 3*Padding);
	[self addChildViewController:self.feedView];
	[self.feedView didMoveToParentViewController:self];
	[self.view addSubview:self.feedView.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)feedTypeChanged {
	if ([self.feedTypeSegment selectedSegmentIndex] == 0) {
		self.feedView.isApps = YES;
	} else {
		self.feedView.isApps = NO;
	}
	[self.feedView typeSwitched];
}
@end
